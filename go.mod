module gitlab.com/mikebird/go-lifx

go 1.15

require (
	github.com/2tvenom/golifx v0.0.0-20181128083437-451b36557cb4
	github.com/labstack/echo/v4 v4.3.0
)
