package main

import (
	"fmt"
	"net/http"

	"github.com/2tvenom/golifx"
	"github.com/labstack/echo/v4"
)

func main() {
	e := echo.New()

	//TODO replace with RegisterBulbs() that will lookup all bulbs and compare to a db
	bulbs, _ := golifx.LookupBulbs()
	fmt.Printf("found %v bulbs\n", len(bulbs))

	//TODO instead of grabbing first bulb, need to make collection of all bulbs, store them somewhere,
	// and then based on URI, grab proper bulb
	// structure for uri room/device/state
	// ex. /parlour/lamp/on or /house/all/off
	bulb := NewBulb(bulbs[0])

	e.GET("/test", func(c echo.Context) error {
		return bulb.Cycle(c)
	})
	e.GET("/on", func(c echo.Context) error {
		bulb.TurnOn()
		return c.String(http.StatusOK, "Light is on!\n")
	})
	e.GET("/off", func(c echo.Context) error {
		bulb.TurnOff()
		return c.String(http.StatusOK, "Light is off!\n")
	})
	e.GET("/brightness/:percentage", func(c echo.Context) error {
		bulb.SetBrightness(c)
		return c.String(http.StatusOK, fmt.Sprintf("Light's brightness is set to %s%s!\n", c.Param("percentage"), "%"))
	})
	e.Logger.Fatal(e.Start(":1323"))

}
