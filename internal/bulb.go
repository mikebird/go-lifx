package main

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/2tvenom/golifx"
	"github.com/labstack/echo/v4"
)

// To extend library functionality
type Bulb struct {
	Lifx *golifx.Bulb
}

func NewBulb(b *golifx.Bulb) *Bulb {
	bs := Bulb{
		Lifx: b,
	}

	return &bs
}

func (b Bulb) Cycle(c echo.Context) error {

	b.TurnOn()

	ticker := time.NewTicker(time.Second)
	counter := 0

	hsbk := &golifx.HSBK{
		Hue:        2000,
		Saturation: 13106,
		Brightness: 65535,
		Kelvin:     3200,
	}
	// Change color every second
	for _ = range ticker.C {
		b.Lifx.SetColorState(hsbk, 500)
		counter++
		hsbk.Hue += 5000
		if counter > 10 {
			ticker.Stop()
			break
		}
	}

	b.TurnOff()
	return c.JSON(http.StatusOK, "OK")
}

func (b Bulb) GetName() string {
	name, _ := b.Lifx.GetLabel()
	return name
}

func (b Bulb) IsOn() bool {
	on, _ := b.Lifx.GetPowerState()
	return on
}

func (b Bulb) TurnOn() {
	if !b.IsOn() {
		fmt.Printf("turning on %s\n", b.GetName())
		b.Lifx.SetPowerState(true)
	} else {
		fmt.Printf("%s is already on \n", b.GetName())
	}
}

func (b Bulb) TurnOff() {
	if b.IsOn() {
		fmt.Printf("Turning off %s\n", b.GetName())
		b.Lifx.SetPowerState(false)
	} else {
		fmt.Printf("%s is already off \n", b.GetName())
	}
}

func (b Bulb) SetBrightness(c echo.Context) error {
	percentage, _ := strconv.ParseInt(c.Param("percentage"), 10, 16)
	// Brightness value is between 0 and 65535
	brightness := uint16(percentage) * 655
	hsbk := &golifx.HSBK{
		Brightness: brightness,
	}
	b.Lifx.SetColorState(hsbk, 1)
	return nil
}
